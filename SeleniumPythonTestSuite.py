import unittest
from tests import Tests

# get all tests from Test class
test_cases = unittest.TestLoader().loadTestsFromTestCase(Tests)

# create a test suite combining search_text and home_page_test
test_suite = unittest.TestSuite([test_cases])

# run the suite
unittest.TextTestRunner(verbosity=2).run(test_suite)