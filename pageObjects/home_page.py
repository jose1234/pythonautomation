from selenium.webdriver.common.by import By


class home_page:

    def __init__(self, driver):
        self.driver = driver

    about = (By.XPATH, "//a[@href='gateway.do?target=aboutUs']")

    def aboutinformation(self):
        return self.driver.find_element(*home_page.about).click()
