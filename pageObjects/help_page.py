from selenium.webdriver.common.by import By


class help_page:

    def __init__(self, driver):
        self.driver = driver

    help = (By.XPATH, "//a[@href ='gateway.do?target=help']")

    def helpinformation(self):
        return self.driver.find_element(*help_page.help).click()
