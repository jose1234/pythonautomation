from selenium.webdriver.common.by import By


class login_page:

    def __init__(self, driver):
        self.driver = driver

    login = (By.XPATH, "//a[@href ='login.do']")
    login_button = (By.ID, "loginButton")
    log_out = (By.CLASS_NAME, "activeLOGOUT")
    logotitle = (By.CLASS_NAME, "logo-title")
    search_button = (By.ID, "fullButton")
    username = "5149482"
    password = "WGVT3J9V37"

    vin = "LYV102DK8KB191538"

    def logininformation(self):
        return self.driver.find_element(*login_page.login).click()

    def loginsubmit(self):
        self.logininformation()
        self.driver.find_element_by_name("username").send_keys(*login_page.username)
        self.driver.find_element_by_name("password").send_keys(*login_page.password)
        return self.driver.find_element(*login_page.login_button).click()

    def vin_search(self):
        self.loginsubmit()
        self.driver.find_element_by_name("vin").send_keys(*login_page.vin)
        return self.driver.find_element(*login_page.search_button).click()

    def log_out_name(self):
        return self.driver.find_element(*login_page.log_out).text

    def logo_title(self):
        return self.driver.find_element(*login_page.logotitle).text
