import logging
import unittest
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from utilities.BaseClass import BaseClass
from utilities.BaseClass import getLogger
from pageObjects.home_page import home_page
from pageObjects.login_page import login_page
from pageObjects.help_page import help_page

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
HOMEPAGE = "https://autocheck.com/members/gateway.do"
log = getLogger()


class Tests(BaseClass):

    def test_homePage(self):
        """
        Method that verify if the Home page shows up
        """
        home_title = self.driver.title
        log.info("Getting the home title page name")
        log.info(home_title)
        assert ("Welcome to the AutoCheck Member's Site" in home_title, "The home title is not equal")
        # self.assertEqual(home_title, "Welcome to the AutoCheck Member's Site", "The home title is not equal")
        print("Test 03 passed")

    def test_about(self):
        """
        Method that verify if the About page shows up
        """
        about = home_page(self.driver)
        about.aboutinformation()
        title = self.driver.title
        assert ("AutoCheck About Us" in title, "The about title is not equal")
        # self.assertEqual(title, "AutoCheck About Us", "The about title is not equal")
        print("Test 01 passed")

    def test_loginPage(self):
        """
        Method that verify if the login page shows up
        """
        loginelement = login_page(self.driver)
        loginelement.logininformation()
        WebDriverWait(self.driver, 5)
        page_title = self.driver.title
        assert ("AutoCheck Login" in page_title, "The login title is not equal")
        # self.assertEqual(page_title, "AutoCheck Login", "The login title is not equal")
        print("Test 02 passed")

    def test_submitLogin(self):
        """
        Method to verify if the submit login works
        """
        loginelement = login_page(self.driver)
        loginelement.loginsubmit()
        result = loginelement.log_out_name()
        assert ("Log Out" in result, "The login submit is not successful")
        # self.assertEqual(result, "Log Out", "The login submit is not successful")
        print("Test 04 passed")

    def test_helpPage(self):
        """
        Method to verify if the help page display
        """
        helpElement = help_page(self.driver)
        helpElement.helpinformation()
        WebDriverWait(self.driver, 5)
        help_title = self.driver.title
        assert ("AutoCheck Help" in help_title, "The help title is not equal")
        # self.assertEqual(help_title, "AutoCheck Help", "The help title is not equal")
        print("Test 05 passed")

    def test_vinSearch(self):
        """
        Method to verify if the vin search works
        """
        loginpage = login_page(self.driver)
        loginpage.loginsubmit()
        result = loginpage.log_out_name()
        if result == 'Log Out':
            loginpage.vin_search()
            message = loginpage.logo_title()
            assert ("Your AutoCheck Vehicle History Report" in message, "The vin dont exist")
            # self.assertEqual(message, "Your AutoCheck Vehicle History Report", "The vin dont exist")
            print("Test 06 passed")
